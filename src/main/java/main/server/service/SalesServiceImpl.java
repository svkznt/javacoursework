package main.server.service;

import main.server.exception.SalesNotFoundException;
import main.server.entity.Sales;
import main.server.repository.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SalesServiceImpl implements SalesService
{
    @Autowired
    private SalesRepository salesRepository;

    @Override
    public List<Sales> listSales()
    {
        return (List<Sales>) salesRepository.findAll();
    }

    @Override
    public Sales findSales(long id)
    {
        Optional<Sales> optionalSales = salesRepository.findById(id);
        if (optionalSales.isPresent())
        {
            return optionalSales.get();
        }
        else
        {
            throw new SalesNotFoundException("Sales not found");
        }
    }

    @Override
    public Sales addSales(Sales sales)
    {
        return salesRepository.save(sales);
    }

    @Override
    public void deleteSalesById(long id)
    {
        salesRepository.deleteById(id);
    }


}
