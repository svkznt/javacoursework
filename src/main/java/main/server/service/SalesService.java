package main.server.service;

import main.server.entity.Sales;

import java.util.List;

public interface SalesService
{
    List<Sales> listSales();
    Sales findSales(long id);
    Sales addSales(Sales sales);
    void deleteSalesById(long id);
}
