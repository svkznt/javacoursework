package main.server.service;

import main.server.entity.Expense_items;

import java.util.List;

public interface Expense_itemsService
{
    List<Expense_items> listExpense_items();
    Expense_items findExpense_items(long id);
    Expense_items addExpense_items(Expense_items expense_items);
    void deleteExpense_itemsById(long id);
}
