package main.server.service;

import main.server.exception.WarehousesNotFoundException;
import main.server.repository.WarehouseRepository;
import main.server.entity.Warehouses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WarehousesServiceImpl implements WarehouseService
{
    @Autowired
    private WarehouseRepository warehouseRepository;

    @Override
    public List<Warehouses> listWarehouses()
    {
        return (List<Warehouses>) warehouseRepository.findAll();
    }

    @Override
    public Warehouses findWarehouses(long id)
    {
        Optional<Warehouses> optionalWarehouses = warehouseRepository.findById(id);
        if (optionalWarehouses.isPresent())
        {
            return optionalWarehouses.get();
        }
        else
        {
            throw new WarehousesNotFoundException("Warehouses not found");
        }
    }

    @Override
    public Warehouses addWarehouses(Warehouses warehouses)
    {
        return warehouseRepository.save(warehouses);
    }

    @Override
    public void deleteWarehousesById(long id)
    {
        warehouseRepository.deleteById(id);
    }
}
