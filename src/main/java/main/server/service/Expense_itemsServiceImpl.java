package main.server.service;

import main.server.repository.Expense_itemsRepository;
import main.server.entity.Expense_items;
import main.server.exception.ExpenseItemsNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Expense_itemsServiceImpl implements Expense_itemsService
{
    @Autowired
    private Expense_itemsRepository expense_itemsRepository;

    @Override
    public List<Expense_items> listExpense_items()
    {
        return (List<Expense_items>) expense_itemsRepository.findAll();
    }

    @Override
    public Expense_items findExpense_items(long id)
    {
        Optional<Expense_items> optionalExpense_items = expense_itemsRepository.findById(id);
        if (optionalExpense_items.isPresent())
        {
            return optionalExpense_items.get();
        }
        else
        {
            throw new ExpenseItemsNotFoundException("Expense_items not found");
        }
    }

    @Override
    public Expense_items addExpense_items(Expense_items expense_items)
    {
        return expense_itemsRepository.save(expense_items);
    }

    @Override
    public void deleteExpense_itemsById(long id)
    {
        expense_itemsRepository.deleteById(id);
    }


}
