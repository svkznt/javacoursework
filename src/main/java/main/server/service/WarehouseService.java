package main.server.service;

import main.server.entity.Warehouses;

import java.util.List;

public interface WarehouseService
{
    List<Warehouses> listWarehouses();
    Warehouses findWarehouses(long id);
    Warehouses addWarehouses(Warehouses warehouses);
    void deleteWarehousesById(long id);
}
