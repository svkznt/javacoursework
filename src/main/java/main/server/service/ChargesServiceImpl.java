package main.server.service;

import main.server.entity.Charges;
import main.server.exception.ChargesNotFoundException;
import main.server.repository.ChargesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChargesServiceImpl implements ChargesService
{
    @Autowired
    private ChargesRepository chargesRepository;

    @Override
    public List<Charges> listCharges()
    {
        return (List<Charges>) chargesRepository.findAll();
    }

    @Override
    public Charges findCharges(long id)
    {
        Optional<Charges> optionalCharges = chargesRepository.findById(id);
        if (optionalCharges.isPresent())
        {
            return optionalCharges.get();
        }
        else
        {
            throw new ChargesNotFoundException("Sales not found");
        }
    }

    @Override
    public Charges addCharges(Charges charges)
    {
        return chargesRepository.save(charges);
    }

    @Override
    public void deleteChargesById(long id)
    {
        chargesRepository.deleteById(id);
    }
}
