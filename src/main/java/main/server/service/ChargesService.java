package main.server.service;

import main.server.entity.Charges;

import java.util.List;

public interface ChargesService
{
    List<Charges> listCharges();
    Charges findCharges(long id);
    Charges addCharges(Charges charges);
    void deleteChargesById(long id);
}
