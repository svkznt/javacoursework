package main.server.security;

import main.server.security.jwt.JwtSecurityConfigurer;
import main.server.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.httpBasic().disable()
                .csrf().disable()
                .formLogin().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/bt/auth/signin").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/sales/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/bt/charges/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/bt/warehouses/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/bt/expense_items/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/bt/addSales").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/bt/addWarehouses").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/bt/addCharges").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/bt/addExpense_items").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/bt/deleteSales/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/bt/deleteCharges/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/bt/deleteWarehouses/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/bt/deleteExpense_items/{id}").hasRole("ADMIN")
                /*.antMatchers(HttpMethod.GET, "/bt/sales/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/charges/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/warehouses/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/expense_items/{id}").permitAll()
                .antMatchers(HttpMethod.POST, "/bt/addSales").permitAll()
                .antMatchers(HttpMethod.POST, "/bt/addWarehouses").permitAll()
                .antMatchers(HttpMethod.POST, "/bt/addCharges").permitAll()
                .antMatchers(HttpMethod.POST, "/bt/addExpense_items").permitAll()
                .antMatchers(HttpMethod.DELETE, "/bt/deleteSales/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE, "/bt/deleteCharges/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE, "/bt/deleteWarehouses/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE, "/bt/deleteExpense_items/{id}").permitAll()*/
                .antMatchers(HttpMethod.GET, "/bt/sales").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/charges").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/warehouses").permitAll()
                .antMatchers(HttpMethod.GET, "/bt/expense_items").permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtSecurityConfigurer(jwtTokenProvider));
    }
}
