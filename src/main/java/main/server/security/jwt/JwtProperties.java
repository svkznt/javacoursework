package main.server.security.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties
{
    private String privateKey = "verySecretKey";
    private long validityInMs = 180000;

    public String getPrivateKey()
    {
        return privateKey;
    }

    public void setPrivateKey(String privateKey)
    {
        this.privateKey = privateKey;
    }

    public long getValidityInMs()
    {
        return validityInMs;
    }

    public void setValidityInMs(long validityInMs)
    {
        this.validityInMs = validityInMs;
    }
}
