package main.server.repository;

import main.server.entity.Expense_items;
import org.springframework.data.repository.CrudRepository;

public interface Expense_itemsRepository extends CrudRepository<Expense_items, Long>
{
}
