package main.server.repository;

import main.server.entity.Charges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ChargesRepository extends CrudRepository<Charges, Long>
{
}
