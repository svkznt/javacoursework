package main.server.repository;

import main.server.entity.Warehouses;
import org.springframework.data.repository.CrudRepository;

public interface WarehouseRepository extends CrudRepository<Warehouses, Long>
{
}
