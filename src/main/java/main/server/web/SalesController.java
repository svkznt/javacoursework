package main.server.web;

import main.server.entity.Sales;
import main.server.exception.SalesNotFoundException;
import main.server.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/bt")
public class SalesController
{
    private SalesService salesService;

    @PostMapping(value = "/addSales", consumes = "application/json", produces = "application/json")
    public Sales addSales(@RequestBody Sales newSales)
    {
        return salesService.addSales(newSales);
    }

    @DeleteMapping("/deleteSales/{id}")
    public void deleteWareSales(@PathVariable("id") long id)
    {
        try
        {
            salesService.deleteSalesById(id);
        }
        catch (SalesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sales not found");
        }
    }

    @GetMapping("/sales")
    public ResponseEntity<List<Sales>> getAllSales()
    {
        List<Sales> listSales = salesService.listSales();
        return new ResponseEntity<>(listSales, HttpStatus.OK);
    }

    @GetMapping("/sales/{id}")
    public ResponseEntity<Sales> getSales(@PathVariable("id") long id)
    {
        try
        {
            return new ResponseEntity<>(salesService.findSales(id), HttpStatus.OK);
        }
        catch (SalesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sales not found");
        }
    }

    @Autowired
    public void setSalesService(SalesService salesService)
    {
        this.salesService = salesService;
    }
}
