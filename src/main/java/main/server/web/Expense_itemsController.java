package main.server.web;

import main.server.entity.Expense_items;
import main.server.exception.ExpenseItemsNotFoundException;
import main.server.service.Expense_itemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/bt")
public class Expense_itemsController
{
    private Expense_itemsService expense_itemsService;

    @PostMapping(value = "/addExpense_items", consumes = "application/json", produces = "application/json")
    public Expense_items addExpense_items(@RequestBody Expense_items newExpense_items)
    {
        return expense_itemsService.addExpense_items(newExpense_items);
    }

    @DeleteMapping("/deleteExpense_items/{id}")
    public void deleteExpense_items(@PathVariable("id") long id)
    {
        try
        {
            expense_itemsService.deleteExpense_itemsById(id);
        }
        catch (ExpenseItemsNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense_items not found");
        }
    }

    @GetMapping("/expense_items")
    public ResponseEntity<List<Expense_items>> getAllExpense_items()
    {
        List<Expense_items> listExpense_items = expense_itemsService.listExpense_items();
        return new ResponseEntity<>(listExpense_items, HttpStatus.OK);
    }

    @GetMapping("/expense_items/{id}")
    public ResponseEntity<Expense_items> getExpense_items(@PathVariable("id") long id)
    {
        try
        {
            return new ResponseEntity<>(expense_itemsService.findExpense_items(id), HttpStatus.OK);
        }
        catch (ExpenseItemsNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense_items not found");
        }
    }

    @Autowired
    public void setExpense_itemsService(Expense_itemsService expense_itemsService)
    {
        this.expense_itemsService = expense_itemsService;
    }
}
