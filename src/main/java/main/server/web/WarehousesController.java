package main.server.web;

import main.server.entity.Warehouses;
import main.server.exception.ChargesNotFoundException;
import main.server.exception.WarehousesNotFoundException;
import main.server.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/bt")
public class WarehousesController
{
    private WarehouseService warehouseService;

    @PostMapping(value = "/addWarehouses", consumes = "application/json", produces = "application/json")
    public Warehouses addWarehouses(@RequestBody Warehouses newWarehouses)
    {
        return warehouseService.addWarehouses(newWarehouses);
    }

    @DeleteMapping("/deleteWarehouses/{id}")
    public void deleteWareWarehouses(@PathVariable("id") long id)
    {
        try
        {
            warehouseService.deleteWarehousesById(id);
        }
        catch (WarehousesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Warehouses not found");
        }
    }

    @GetMapping("/warehouses")
    public ResponseEntity<List<Warehouses>> getAllWarehouses()
    {
        List<Warehouses> listWarehouses = warehouseService.listWarehouses();
        return new ResponseEntity<>(listWarehouses, HttpStatus.OK);
    }

    @GetMapping("/warehouses/{id}")
    public ResponseEntity<Warehouses> getWarehouses(@PathVariable("id") long id)
    {
        try
        {
            return new ResponseEntity<>(warehouseService.findWarehouses(id), HttpStatus.OK);
        }
        catch (ChargesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Charges not found");
        }
    }

    @Autowired
    public void setWarehouseService(WarehouseService warehouseService)
    {
        this.warehouseService = warehouseService;
    }
}
