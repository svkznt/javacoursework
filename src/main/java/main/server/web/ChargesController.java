package main.server.web;

import main.server.entity.Charges;
import main.server.exception.ChargesNotFoundException;
import main.server.service.ChargesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/bt")
public class ChargesController
{
    private ChargesService chargesService;

    @PostMapping(value = "/addCharges", consumes = "application/json", produces = "application/json")
    public Charges addCharges(@RequestBody Charges newCharges)
    {
        return chargesService.addCharges(newCharges);
    }

    @DeleteMapping("/deleteCharges/{id}")
    public void deleteCharges(@PathVariable("id") long id)
    {
        try
        {
            chargesService.deleteChargesById(id);
        }
        catch (ChargesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Charges not found");
        }
    }

    @GetMapping("/charges")
    public ResponseEntity<List<Charges>> getAllCharges()
    {
        List<Charges> listCharges = chargesService.listCharges();
        return new ResponseEntity<>(listCharges, HttpStatus.OK);
    }

    @GetMapping("/charges/{id}")
    public ResponseEntity<Charges> getCharges(@PathVariable("id") long id)
    {
        try
        {
            return new ResponseEntity<>(chargesService.findCharges(id), HttpStatus.OK);
        }
        catch (ChargesNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Charges not found");
        }
    }

    @Autowired
    public void setChargesService(ChargesService chargesService)
    {
        this.chargesService = chargesService;
    }
}
