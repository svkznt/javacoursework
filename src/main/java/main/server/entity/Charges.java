package main.server.entity;

import javax.validation.constraints.NotNull;
import javax.persistence.*;

@Entity
@Table(name = "charges")
public class Charges
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "price")
    @NotNull(message = "Price is required")
    private Double price;

    @Column(name = "charge_date")
    @NotNull(message = "Charge_date is required")
    private String charge_date;

    @Column(name = "expense_item_id")
    @NotNull(message = "Expense_item_id is required")
    private long expense_item_id;

    @ManyToOne(targetEntity = Expense_items.class)
    @NotNull(message = "Expense_items are required")
    private Expense_items expense_items;

    public Charges()
    {
    }

    public Expense_items getExpense_items() {
        return expense_items;
    }

    public void setExpense_items(Expense_items expense_items) {
        this.expense_items = expense_items;
    }

    public Charges(Double price, String charge_date, long expense_item_id, Expense_items expense_items)
    {
        this.price = price;
        this.charge_date = charge_date;
        this.expense_item_id = expense_item_id;
        this.expense_items = expense_items;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Double getPrice()
    {
        return price;
    }

    public void setPrice(Double price)
    {
        this.price = price;
    }

    public String getCharge_date()
    {
        return charge_date;
    }

    public void setCharge_date(String charge_date)
    {
        this.charge_date = charge_date;
    }

    public long getExpense_item_id()
    {
        return expense_item_id;
    }

    public void setExpense_item_id(long expense_item_id)
    {
        this.expense_item_id = expense_item_id;
    }
}
