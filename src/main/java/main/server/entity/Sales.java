package main.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sales")
public class Sales
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "price")
    @NotNull(message = "Price is required")
    private Double price;

    @Column(name = "quantity")
    @NotNull(message = "Quantity is required")
    private Integer quantity;

    @Column(name = "sale_date")
    @NotNull(message = "Sale_date is required")
    private String sale_date;

    @Column(name = "warehouse_id")
    @NotNull(message = "Warehouse_id is required")
    private long warehouse_id;

    @ManyToOne(targetEntity = Warehouses.class)
    @NotNull(message = "Warehouses are required")
    private Warehouses warehouses;

    public Sales()
    {
    }

    public Sales(Double price, Integer quantity, String sale_date, long warehouse_id, Warehouses warehouses)
    {
        this.price = price;
        this.quantity = quantity;
        this.sale_date = sale_date;
        this.warehouse_id = warehouse_id;
        this.warehouses = warehouses;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Warehouses getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Warehouses warehouses) {
        this.warehouses = warehouses;
    }

    public long getId()
    {
        return id;
    }
    public void setPrice(Double price)
    {
        this.price = price;
    }

    public Double getPrice()
    {
        return price;
    }

    public void setQuantity(Integer quantity)
    {
        this.quantity = quantity;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public void setSaleDate(String sale_date)
    {
        this.sale_date = sale_date;
    }

    public String getSaleDate()
    {
        return sale_date;
    }

    public void setWarehouseId(long warehouse_id)
    {
        this.warehouse_id = warehouse_id;
    }

    public long getWarehouseId()
    {
        return warehouse_id;
    }

    @Override
    public String toString()
    {
        return "Sales{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                ", sale_date=" + sale_date +
                ", warehouse_id" + warehouse_id + "}";
    }
}
