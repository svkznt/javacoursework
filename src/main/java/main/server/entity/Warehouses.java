package main.server.entity;

import javax.validation.constraints.NotNull;
import javax.persistence.*;

@Entity
@Table(name = "warehouses")
public class Warehouses
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    @NotNull(message = "Name is required")
    private String name;

    @Column(name = "quantity")
    @NotNull(message = "Quantity is required")
    private Integer quantity;

    @Column(name = "price")
    @NotNull(message = "Price is required")
    private Double price;

    public Warehouses()
    {
    }

    public Warehouses(String name, Integer quantity, Double price)
    {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public long getId()
    {
        return id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setQuantity(Integer quantity)
    {
        this.quantity = quantity;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public Double getPrice()
    {
        return price;
    }

    public void setPrice(Double price)
    {
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "Warehouses{" +
                "id=" + id +
                ", name=" + name +
                ", quantity=" + quantity +
                ", price=" + price + "}";
    }
}
