package main.server.entity;

import javax.validation.constraints.NotNull;
import javax.persistence.*;

@Entity
@Table(name = "expense_items")
public class Expense_items
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    @NotNull(message = "Name is required")
    private String name;

    public Expense_items()
    {
    }

    public Expense_items(String name)
    {
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
