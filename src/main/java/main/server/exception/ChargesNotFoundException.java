package main.server.exception;

public class ChargesNotFoundException extends RuntimeException
{
    public ChargesNotFoundException(String message)
    {
        super(message);
    }
}
