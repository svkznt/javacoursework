package main.server.exception;

public class ExpenseItemsNotFoundException extends RuntimeException
{
    public ExpenseItemsNotFoundException(String message)
    {
        super(message);
    }
}
