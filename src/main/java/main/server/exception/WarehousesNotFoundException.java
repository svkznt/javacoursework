package main.server.exception;

public class WarehousesNotFoundException extends RuntimeException
{
    public WarehousesNotFoundException(String message)
    {
        super(message);
    }
}
