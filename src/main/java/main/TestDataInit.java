package main;

import main.server.entity.*;
import main.server.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class TestDataInit implements CommandLineRunner
{
    @Autowired
    SalesRepository salesRepository;

    @Autowired
    Expense_itemsRepository expense_itemsRepository;

    @Autowired
    ChargesRepository chargesRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    WarehouseRepository warehouseRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception
    {
        Expense_items expense_items1 = new Expense_items("First");
        Expense_items expense_items2 = new Expense_items("Second");
        Expense_items expense_items3 = new Expense_items("Third");
        Expense_items expense_items4 = new Expense_items("Fourth");
        Expense_items expense_items5 = new Expense_items("Fifth");
        Expense_items expense_items6 = new Expense_items("Sixth");
        expense_itemsRepository.save(expense_items1);
        expense_itemsRepository.save(expense_items2);
        expense_itemsRepository.save(expense_items3);
        expense_itemsRepository.save(expense_items4);
        expense_itemsRepository.save(expense_items5);
        expense_itemsRepository.save(expense_items6);
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items1.getId(), expense_items1));
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items2.getId(), expense_items2));
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items3.getId(), expense_items3));
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items4.getId(), expense_items4));
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items5.getId(), expense_items5));
        chargesRepository.save(new Charges((double) 10, "15.07.20", expense_items6.getId(), expense_items6));

        Warehouses warehouses1 = new Warehouses("firsttt", 10, 1000.0);
        Warehouses warehouses2 = new Warehouses("seconddd", 20, 2000.0);
        Warehouses warehouses3 = new Warehouses("third", 30, 3000.0);
        Warehouses warehouses4 = new Warehouses("fourth", 40, 4000.0);
        Warehouses warehouses5 = new Warehouses("fifth", 50, 5000.0);
        Warehouses warehouses6 = new Warehouses("sixth", 60, 6000.0);
        Warehouses warehouses7 = new Warehouses("seventh", 70, 7000.0);
        Warehouses warehouses8 = new Warehouses("sdghhsh", 70, 7000.0);
        Warehouses warehouses9 = new Warehouses("seshdfbventh", 70, 7000.0);
        Warehouses warehouses10 = new Warehouses("sesdfhdfujjventh", 70, 7000.0);
        warehouseRepository.save(warehouses1);
        warehouseRepository.save(warehouses2);
        warehouseRepository.save(warehouses3);
        warehouseRepository.save(warehouses4);
        warehouseRepository.save(warehouses5);
        warehouseRepository.save(warehouses6);
        warehouseRepository.save(warehouses7);
        warehouseRepository.save(warehouses8);
        warehouseRepository.save(warehouses9);
        warehouseRepository.save(warehouses10);
        salesRepository.save(new Sales(warehouses1.getPrice(), warehouses1.getQuantity(), "15.07.2000", warehouses1.getId(), warehouses1));
        salesRepository.save(new Sales(warehouses2.getPrice(), warehouses2.getQuantity(), "16.07.2000", warehouses2.getId(), warehouses2));
        salesRepository.save(new Sales(warehouses3.getPrice(), warehouses3.getQuantity(), "17.07.2000", warehouses3.getId(), warehouses3));
        salesRepository.save(new Sales(warehouses4.getPrice(), warehouses4.getQuantity(), "18.07.2000", warehouses4.getId(), warehouses4));
        salesRepository.save(new Sales(warehouses5.getPrice(), warehouses5.getQuantity(), "19.07.2000", warehouses5.getId(), warehouses5));
        salesRepository.save(new Sales(warehouses6.getPrice(), warehouses6.getQuantity(), "20.07.2000", warehouses6.getId(), warehouses6));
        salesRepository.save(new Sales(warehouses7.getPrice(), warehouses7.getQuantity(), "21.07.2000", warehouses7.getId(), warehouses7));

        userRepository.save(new User("user", passwordEncoder.encode("pwd"), Collections.singletonList("ROLE_USER")));
        userRepository.save(new User("admin", passwordEncoder.encode("apwd"), Collections.singletonList("ROLE_ADMIN")));
    }
}
